Subroutine Get_Gaussmu
    use blockc
    use spring
    implicit none

    ! local parameter
    integer :: i, j, m
    real(dp) :: en_free
    complex(dp) :: z0, z1
    complex(dp), dimension(:,:), allocatable :: hvec
    real(dp), dimension(:),allocatable :: heig
    
    allocate( hvec(ndim, ndim) )
    allocate(heig(ndim))

    !write(6,*) 'hop_up'
    do i = 1, ndim
        hop_up(i,i) = - dcmplx(spring_sfmt_gaussian(mu,mu_var),0.d0)
    !    write(6,*) hop_up(i,i)
    enddo

    call s_eig_he(ndim, ndim, hop_up, heig, hvec)
    !write(6,*) "hop_up"
    do i = 1,ndim
        do j = 1,ndim
           z0 = dcmplx(0.d0,0.d0)
           z1 = dcmplx(0.d0,0.d0)
           do m = 1,ndim
              z0 = z0 +  hvec(i,m) * dcmplx(dexp(-dtau *heig(m)),0.d0) * dconjg(hvec(j,m))
              z1 = z1 +  hvec(i,m) * dcmplx(dexp( dtau *heig(m)),0.d0) * dconjg(hvec(j,m))
           enddo
           urt  (i,j) = z0
           urtm1(i,j) = z1
    !!       write(6,*) i,j,z0
        enddo
     enddo
     !write(6,*) ''
   
     if( irank.eq.0 ) then
        write(fout,*)
        write(fout,'(a)') ' heig(:) = '
        do i = 1, ndim
            write(fout,'(e16.8)') heig(i)
        end do
  
        en_free = 0.d0
        do i = 1, ndim/2
            en_free = en_free + heig(i)
        end do
        write(fout,*)
        write(fout,'(a,e16.8)') ' half-filling en_free = ', 2.d0*en_free   ! 2 spin flavor
    end if
#ifdef SPINDOWN
    do i = 1, ndim
        hop_dn(i,i) = hop_dn(i,i) - dcmplx(spring_sfmt_gaussian(mu,1.d0),0.d0)
    enddo

    call s_eig_he(ndim,ndim,hop_dn,heig,hvec)
    do i = 1,ndim
       do j = 1,ndim
          z0 = dcmplx(0.d0,0.d0)
          z1 = dcmplx(0.d0,0.d0)
          do m = 1,ndim
             z0 = z0 +  hvec(i,m) * dcmplx(dexp(-dtau *heig(m)),0.d0) * dconjg(hvec(j,m))
             z1 = z1 +  hvec(i,m) * dcmplx(dexp( dtau *heig(m)),0.d0) * dconjg(hvec(j,m))
          enddo
          urt_dn  (i,j) = z0
          urtm1_dn(i,j) = z1
       enddo
    enddo
#endif
deallocate(hvec)
deallocate(heig)
  
endSubroutine Get_Gaussmu
